set month May
set weekday Friday
set day 5
set extension th
set year 2010
set my_string [format "Today is %s, %s %d%s %d" $weekday $month $day $extension $year]
puts $my_string