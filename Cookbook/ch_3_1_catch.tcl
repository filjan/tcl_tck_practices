puts -nonewline "Enter a number: "
flush stdout
gets stdin value
if {[catch {set doubled [ expr $value * 2]} errmsg]} then {
	puts "Script Failed - $errmsg"
} else {
	puts "$value doubled is: $doubled"
}

puts "Regardless of error the script continues"