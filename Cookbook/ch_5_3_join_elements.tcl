set list_1 [list 1 2 3 4]
puts $list_1
set list_2 [list 1 "2 3" 4 " "]
puts $list_2
set list_3 [concat $list_1 $list_2 $list_1] 
puts $list_3

set list_4 [join $list_3 "@"] 
puts $list_4
