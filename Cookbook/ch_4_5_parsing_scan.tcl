# Convert hexadecimal color symbol to r/g/b
set color #34aa44
scan $color #%2x%2x%2x r g b
puts "$r $g $b"