set list_1 [list 1 "2 3" 4 " "] 
puts [lsearch $list_1 4]

puts [lsearch -sorted -decreasing $list_1 1]
puts [lsearch -sorted -increasing $list_1 1]

set list_2 [list 1 "2 3" 4 " " 1] 
puts [lsearch -all -sorted -increasing $list_2 1]
puts [lsearch -all -sorted -decreasing $list_2 1]
